'use strict';

/**
 * @ngdoc overview
 * @name truckSahayakUiApp
 * @description
 * # truckSahayakUiApp
 *
 * Main module of the application.
 */
angular
  .module('truckSahayakUiApp', [
    'ui.router'
  ])
  .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');

    $stateProvider
      .state('home', {
        url:'/',
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
      })
      .state('driver', {
        url:'/driver',
        templateUrl: 'views/driver.html',
        controller: 'Driverctrl as driver',
      })
      .state('truck', {
        url:'/truck',
        templateUrl: 'views/truck.html',
        controller: 'Truckctrl',
      })
      .state('gate', {
        url:'/gate',
        templateUrl: 'views/gate.html',
        controller: 'Gatectrl',
      })
      .state('trip', {
        url:'/trip',
        templateUrl: 'views/trip.html',
        controller: 'Tripctrl',
      });
  }]);
