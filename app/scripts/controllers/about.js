'use strict';

/**
 * @ngdoc function
 * @name truckSahayakUiApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the truckSahayakUiApp
 */
angular.module('truckSahayakUiApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
