'use strict';

/**
 * @ngdoc function
 * @name truckSahayakUiApp.controller:DriverctrlCtrl
 * @description
 * # DriverctrlCtrl
 * Controller of the truckSahayakUiApp
 */
angular.module('truckSahayakUiApp')
  .controller('Driverctrl', ['DriverSrv', function (driverSrv) {
    this.drivers = driverSrv.drivers;
  }]);
