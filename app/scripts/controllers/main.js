'use strict';

/**
 * @ngdoc function
 * @name truckSahayakUiApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the truckSahayakUiApp
 */
angular.module('truckSahayakUiApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
