'use strict';

/**
 * @ngdoc service
 * @name truckSahayakUiApp.DriverSrv
 * @description
 * # DriverSrv
 * Service in the truckSahayakUiApp.
 */
angular.module('truckSahayakUiApp')
  .service('DriverSrv', function () {
    // AngularJS will instantiate a singleton by calling "new" on this function
    this.drivers = [ 
    	{	'name' : 'Akash', 'number' : '88888'}, 
    	{   'name' : 'Nisar', 'number' : '88889'}
    ];
  });
