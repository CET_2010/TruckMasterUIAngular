'use strict';

describe('Controller: DriverctrlCtrl', function () {

  // load the controller's module
  beforeEach(module('truckSahayakUiApp'));

  var DriverctrlCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DriverctrlCtrl = $controller('DriverctrlCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(DriverctrlCtrl.awesomeThings.length).toBe(3);
  });
});
