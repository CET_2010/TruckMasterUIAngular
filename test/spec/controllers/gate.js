'use strict';

describe('Controller: GatectrlCtrl', function () {

  // load the controller's module
  beforeEach(module('truckSahayakUiApp'));

  var GatectrlCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    GatectrlCtrl = $controller('GatectrlCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(GatectrlCtrl.awesomeThings.length).toBe(3);
  });
});
