'use strict';

describe('Controller: TripctrlCtrl', function () {

  // load the controller's module
  beforeEach(module('truckSahayakUiApp'));

  var TripctrlCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TripctrlCtrl = $controller('TripctrlCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(TripctrlCtrl.awesomeThings.length).toBe(3);
  });
});
