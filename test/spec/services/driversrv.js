'use strict';

describe('Service: DriverSrv', function () {

  // load the service's module
  beforeEach(module('truckSahayakUiApp'));

  // instantiate service
  var DriverSrv;
  beforeEach(inject(function (_DriverSrv_) {
    DriverSrv = _DriverSrv_;
  }));

  it('should do something', function () {
    expect(!!DriverSrv).toBe(true);
  });

});
